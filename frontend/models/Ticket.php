<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "ticket".
 *
 * @property int $id
 * @property string $subject
 * @property string $from
 * @property string|null $body
 * @property string|null $date
 * @property integer|null $status
 */
class Ticket extends \yii\db\ActiveRecord
{
    const STATUS_CLOSED = 2;
    const STATUS_WAIT = 1;
    const STATUS_NEW = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ticket';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subject', 'from'], 'required'],
            [['body'], 'string'],
            [['subject', 'from'], 'string', 'max' => 255],
            [['date'], 'string', 'max' => 100],
            [['status'], 'number']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'subject' => Yii::t('app', 'Subject'),
            'from' => Yii::t('app', 'From'),
            'body' => Yii::t('app', 'Body'),
            'date' => Yii::t('app', 'Date'),
            'statusLabel' => Yii::t('app', 'Status'),
        ];
    }

    public function getStatusLabel()
    {
        switch ($this->status) {
            case self::STATUS_WAIT:
                return '<span class="badge badge-primary">Waiting</span>';
            case self::STATUS_CLOSED:
                return '<span class="badge badge-success">Closed</span>';
            default:
                return '<span class="badge badge-info">New</span>';
        }
    }
}

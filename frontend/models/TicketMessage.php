<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "ticket_message".
 *
 * @property int $id
 * @property int $ticket_id
 * @property string|null $message
 * @property string|null $from
 * @property string|null $date
 *
 * @property Ticket $ticket
 */
class TicketMessage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ticket_message';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ticket_id'], 'required'],
            [['ticket_id'], 'integer'],
            [['message'], 'string'],
            [['date'], 'safe'],
            [['from'], 'string', 'max' => 255],
            [['ticket_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ticket::className(), 'targetAttribute' => ['ticket_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ticket_id' => Yii::t('app', 'Ticket ID'),
            'message' => Yii::t('app', 'Message'),
            'from' => Yii::t('app', 'From'),
            'date' => Yii::t('app', 'Date'),
        ];
    }

    /**
     * Gets query for [[Ticket]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTicket()
    {
        return $this->hasOne(Ticket::className(), ['id' => 'ticket_id']);
    }
}

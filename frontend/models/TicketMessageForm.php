<?php

namespace frontend\models;

use common\services\MailingService;
use Yii;
use yii\base\Model;

class TicketMessageForm extends Model
{
    public $from;
    public $subject;
    public $body;

    public function rules()
    {
        return [
            [['from', 'subject', 'body'], 'required'],
        ];
    }

    public function sendMail($ticket_id)
    {
        $model = new TicketMessage();
        $model->from = Yii::$app->params['imap']['username'];
        $model->message = $this->body;
        $model->ticket_id = $ticket_id;
        if (!$model->save())
            return false;
        $service = new MailingService();
        return $service->sendCustomTicketMail($this->from, $this->subject, $this->body);
    }
}
<?php

/* @var $this yii\web\View */

$this->title = 'Home';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Greetings!</h1>

        <p class="lead">It`s the main page.</p>

        <p><a class="btn btn-lg btn-success" href="<?= \yii\helpers\Url::to(['/ticket']) ?>">Show tickets</a></p>
    </div>

</div>

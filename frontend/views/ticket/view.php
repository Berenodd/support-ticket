<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Ticket */

$this->title = $model->subject;
$this->params['breadcrumbs'][] = ['label' => 'Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ticket-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'subject',
            'from',
            'statusLabel:raw',
            'date',
        ],
    ]) ?>
    <?php if ($model->status !== \frontend\models\Ticket::STATUS_CLOSED) : ?>
        <a href="<?= \yii\helpers\Url::to(['ticket/close', 'id' => $model->id]) ?>" class="btn btn-success">Close
            ticket</a>
    <?php endif; ?>
    <hr>
    <h3>Messages:</h3>

    <div class="messages-wrapper">
        <div class="messages-body">
            <?php if (isset($messages)) { ?>
                <?php foreach ($messages as $message) : ?>
                    <div class="message-item <?= $message->from !== $model->from ? 'output' : '' ?>">
                        <div class="message-body"><?= $message->message ?></div>
                        <div class="message-date"><?= $message->date ?></div>
                    </div>

                <?php endforeach; ?>
            <?php } else { ?>
                Nothing here yet
            <?php } ?>
        </div>
        <div class="messages-form">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <?php $form = ActiveForm::begin(); ?>
                    <?= $form->field($form_model, 'from')->hiddenInput(['value' => $model->from])->label(false); ?>
                    <?= $form->field($form_model, 'subject') ?>
                    <?= $form->field($form_model, 'body')->textarea(['rows' => 6]) ?>
                    <div class="form-group">
                        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </div>
</div>

<style>
    .messages-body {
        max-height: 300px;
        overflow-y: auto;
    }
    .messages-body {
        background-color: #eee;
        padding: 10px 4px;
    }
    .message-item {
        padding: 4px 14px;
        background-color: #0f6674;
        color: #fff;
        margin-bottom: 20px;
        width: fit-content;
        border-radius: 8px;
    }
    .message-item.output {
        background-color: #1c7430;
        margin-left: auto;
    }
    .message-item .message-date {
        color: #FFFFFF5B;
        font-size: 12px;
    }
</style>

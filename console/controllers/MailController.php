<?php

namespace console\controllers;

use common\services\MailingService;
use yii\console\Controller;

class MailController extends Controller
{
    public function actionUpdate()
    {
        $service = new MailingService();
        $service->checkNewEmail();
    }
}
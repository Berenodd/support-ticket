<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%ticket}}`.
 */
class m211221_224933_add_status_column_to_ticket_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%ticket}}', 'status', $this->integer(1)->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%ticket}}', 'status');
    }
}

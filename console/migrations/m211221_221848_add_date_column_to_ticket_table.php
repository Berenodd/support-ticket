<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%ticket}}`.
 */
class m211221_221848_add_date_column_to_ticket_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%ticket}}', 'date', $this->string(100));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%ticket}}', 'date');
    }
}

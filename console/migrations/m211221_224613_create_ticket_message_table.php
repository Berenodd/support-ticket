<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ticket_message}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%ticket}}`
 */
class m211221_224613_create_ticket_message_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ticket_message}}', [
            'id' => $this->primaryKey(),
            'ticket_id' => $this->integer(11)->notNull(),
            'message' => $this->text(),
            'from' => $this->string(255),
            'date' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);

        // creates index for column `ticket_id`
        $this->createIndex(
            '{{%idx-ticket_message-ticket_id}}',
            '{{%ticket_message}}',
            'ticket_id'
        );

        // add foreign key for table `{{%ticket}}`
        $this->addForeignKey(
            '{{%fk-ticket_message-ticket_id}}',
            '{{%ticket_message}}',
            'ticket_id',
            '{{%ticket}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%ticket}}`
        $this->dropForeignKey(
            '{{%fk-ticket_message-ticket_id}}',
            '{{%ticket_message}}'
        );

        // drops index for column `ticket_id`
        $this->dropIndex(
            '{{%idx-ticket_message-ticket_id}}',
            '{{%ticket_message}}'
        );

        $this->dropTable('{{%ticket_message}}');
    }
}

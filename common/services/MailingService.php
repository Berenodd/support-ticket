<?php

namespace common\services;

use frontend\models\Ticket;
use frontend\models\TicketMessage;
use Yii;
use yii\base\BaseObject;

class MailingService
{
    private $hostname;
    private $username;
    private $password;

    public function __construct()
    {
        $this->hostname = Yii::$app->params['imap']['hostname'];
        $this->username = Yii::$app->params['imap']['username'];
        $this->password = Yii::$app->params['imap']['password'];
    }

    public function checkNewEmail()
    {
        $inbox = imap_open($this->hostname, $this->username, $this->password) or die('Cannot connect to Gmail: ' . imap_last_error());

        $date = date('j F Y', time() - 60 * 60 * 24);
        $emails = imap_search($inbox, 'SINCE "' . $date . '"');

        if ($emails) {
            foreach ($emails as $email_number) {
                $overview = imap_fetch_overview($inbox, $email_number, 0);
                $message = imap_fetchbody($inbox, $email_number, 2);

                $subject = imap_utf8($overview[0]->subject);
                $from = imap_utf8($overview[0]->from);
                $date = imap_utf8($overview[0]->date);
                $body = imap_utf8($message);

                $model = null;
                $this->findTicketBySubject($subject, $from, $model);

                if (!$model)
                    $this->findTicketByIdInSubject($subject, $model);

                if (!$model) {
                    $model = new Ticket();
                    $model->subject = $subject;
                    $model->from = $from;
                    $model->date = $date;
                    $model->body = $body;
                    if (!$model->save()) {
                        Yii::error($model->getErrors());
                        continue;
                    }
                    $this->sendCreateTicketMail($from, $model);
                }

                if (!TicketMessage::find()->where(['ticket_id' => $model->id, 'from' => $from, 'message' => $body])->one()) {
                    $ticket_message_model = new TicketMessage();
                    $ticket_message_model->ticket_id = $model->id;
                    $ticket_message_model->from = $from;
                    $ticket_message_model->message = $body;
                    if (!$ticket_message_model->save()) {
                        Yii::error($ticket_message_model->getErrors());
                        continue;
                    }
                    $model->status = Ticket::STATUS_NEW;
                    $model->save();
                }

            }
        }
        imap_close($inbox);
    }

    public function findTicketByIdInSubject($subject, &$model): void
    {
        preg_match('/(?<=ticket_).*/', $subject, $ticket_id);
        if (isset($ticket_id[0])) {
            $ticket_id = str_replace('ticket_', '', $ticket_id[0]);
            $model = Ticket::findOne($ticket_id);
        }
    }

    private function findTicketBySubject($subject, $from, &$model): void
    {
        $model = Ticket::find()->where(['subject' => $subject, 'from' => $from])->andWhere(['!=', 'status', Ticket::STATUS_CLOSED])->one();
    }

    private function sendCreateTicketMail($from, $model)
    {
        preg_match('/(?<=<).*(?=>)/', $from, $matches);
        if (isset($matches[0])) {
            Yii::$app->mailer->compose()
                ->setFrom(Yii::$app->params['imap']['username'])
                ->setTo($matches[0])
                ->setSubject(Yii::t('app', "Ticket has been created! ID: ticket_$model->id"))
                ->setTextBody(Yii::t('app', 'Thank you! Your request will be processed by support.'))
                ->send();
        }
    }

    public function sendCustomTicketMail($to, $subject, $text)
    {
        preg_match('/(?<=<).*(?=>)/', $to, $matches);
        if (isset($matches[0])) {
            Yii::$app->mailer->compose()
                ->setFrom(Yii::$app->params['imap']['username'])
                ->setTo($matches[0])
                ->setSubject($subject)
                ->setTextBody($text)
                ->send();
            return true;
        }
        return false;
    }
}